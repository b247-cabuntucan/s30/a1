/**
1. Create an activity.js file on where to write and save the solution for the activity.
[done]2. Use the count operator to count the total number of fruits on sale.
[done]3. Use the count operator to count the total number of fruits with stock more than or equal to  20.
[done]4. Use the average operator to get the average price of fruits onSale per supplier.
5. Use the max operator to get the highest price of a fruit per supplier.
6. Use the min operator to get the lowest price of a fruit per supplier.
7. Create a git repository named s30.
 */


//Output 1
db.fruits.aggregate([
    {$match: { onSale: true }},
    {$count: "fruits on sale"},
]);

//Output 2
db.fruits.aggregate([
    {$match: { stock: {$gte: 20} }},
    {$count: "more than 20 stocks"},
]);

//Output 3
db.fruits.aggregate([
    {$match: { onSale: true }},
    {$group: {
        _id: "$supplier_id",
        avg_price: {$avg: "$price"}
    }}
]);

//Output 4
db.fruits.aggregate([
    {$group: {
        _id: "$supplier_id",
        max_price: {$max: "$price"}
    }}
]);

//Output 5
db.fruits.aggregate([
    {$group: {
        _id: "$supplier_id",
        max_price: {$min: "$price"}
    }}
]);